﻿using NUnit.Framework;

namespace TicTacToeKata.test {

/*
Rules

In random order:
    (OK) a game has nine fields in a 3x3 grid                    
    (OK) there are two players in the game (X and O)
    (**) players take turns taking fields until the game is over 
    (OK) a player can take a field if not already taken    
    (OK) a game is over when all fields...         
     (OK)    ... are taken                         
     (OK)    ... in a row are taken by a player    
     (OK)    ... in a column are taken by a player 
     (OK)    ... in a diagonal are taken by a player 
*/
    [TestFixture]
    public class TicTacToeShould {

        private Board _board;

        [SetUp]
        public void SetUp() {
            _board = new Board();
        }

        private Position position(string position) {
            return new Position(position);
        }
        
        [Test]
        public void HasABoardWithNinePositions() {
            Assert.True(_board.ExistPosition(position("A1")));
            Assert.True(_board.ExistPosition(position("A2")));
            Assert.True(_board.ExistPosition(position("A3")));
            Assert.True(_board.ExistPosition(position("B1")));
            Assert.True(_board.ExistPosition(position("B2")));
            Assert.True(_board.ExistPosition(position("B3")));
            Assert.True(_board.ExistPosition(position("C1")));
            Assert.True(_board.ExistPosition(position("C2")));
            Assert.True(_board.ExistPosition(position("C3")));
            Assert.False(_board.ExistPosition(position("A0")));
            Assert.False(_board.ExistPosition(position("B0")));
            Assert.False(_board.ExistPosition(position("C0")));
            Assert.False(_board.ExistPosition(position("A4")));
            Assert.False(_board.ExistPosition(position("B4")));
            Assert.False(_board.ExistPosition(position("C4")));
        }

        [Test]
        public void AllowAPlayerToTakeAnEmptyPosition() {
            Assert.DoesNotThrow(() => _board.TakePosition(new Position("B2"), Player.X));
        }

        [Test]
        public void NotAllowAPlayerToTakeAPositionIfAlreadyTaken() {
            const string position = "B2";
            _board.TakePosition(new Position(position), Player.X);
            Assert.Throws<PositionAlreadyTakenException>(() => _board.TakePosition(new Position(position), Player.O));
        }
        
        [Test]
        public void KnownIfAllPositionsAreTaken() {
            _board.TakePosition(new Position("A1"), Player.O);
            _board.TakePosition(new Position("A2"), Player.X);
            _board.TakePosition(new Position("A3"), Player.O);
            _board.TakePosition(new Position("B1"), Player.X);
            _board.TakePosition(new Position("B2"), Player.O);
            _board.TakePosition(new Position("B3"), Player.X);
            _board.TakePosition(new Position("C1"), Player.O);
            _board.TakePosition(new Position("C2"), Player.X);
            _board.TakePosition(new Position("C3"), Player.O);
            Assert.False(_board.ExistFreePositions());
        }

        [Test]
        public void KnowIfARowIsTakenByAPlayer() {
            const string row = "A";
            _board.TakePosition(new Position(row + "1"), Player.O);
            _board.TakePosition(new Position(row + "2"), Player.O);
            _board.TakePosition(new Position(row + "3"), Player.O);
            Assert.True(_board.IsThereAWinner());
        }
        
        [Test]
        public void KnowIfARowIsNotTakenByAPlayer() {
            const string row = "A";
            _board.TakePosition(new Position(row + "1"), Player.O);
            _board.TakePosition(new Position(row + "2"), Player.X);
            _board.TakePosition(new Position(row + "3"), Player.O);
            Assert.False(_board.IsThereAWinner());
        }

        [Test]
        public void KnowIfAColumnIsTakenByAPlayer() {
            const string column = "1";
            _board.TakePosition(new Position("A" + column), Player.O);
            _board.TakePosition(new Position("B" + column), Player.O);
            _board.TakePosition(new Position("C" + column), Player.O);
            Assert.True(_board.IsThereAWinner());
        }
        
        [Test]
        public void KnowIfAColumnIsNotTakenByAPlayer() {
            const string column = "1";
            _board.TakePosition(new Position("A" + column), Player.O);
            _board.TakePosition(new Position("B" + column), Player.X);
            _board.TakePosition(new Position("C" + column), Player.O);
            Assert.False(_board.IsThereAWinner());
        }

        [Test]
        public void KnowIfADiagonalIsTakenByAPlayer() {
            _board.TakePosition(new Position("A1"), Player.O);
            _board.TakePosition(new Position("B2"), Player.O);
            _board.TakePosition(new Position("C3"), Player.O);
            Assert.True(_board.IsThereAWinner());
        }
        
        [Test]
        public void KnowIfADiagonalIsNotTakenByAPlayer() {
            _board.TakePosition(new Position("A3"), Player.O);
            _board.TakePosition(new Position("B2"), Player.X);
            _board.TakePosition(new Position("C1"), Player.O);
            Assert.False(_board.IsThereAWinner());
        }

    }

}