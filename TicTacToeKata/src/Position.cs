﻿namespace TicTacToeKata {

    public class Position {

        private readonly char _row;
        private readonly char _column;

        public Position(string position) : this(position[0], position[1]) {
        }

        public Position(char row, char column) {
            _row = row;
            _column = column;
        }

        public bool IsInRow(char row) {
            return _row == row;
        }
        
        public bool IsInColumn(char column) {
            return _column == column;
        }

        public override string ToString() {
            return _row + "" + _column;
        }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            var other = (Position) obj;
            return other._column == _column && other._row == _row;
        }

        public override int GetHashCode() {
            unchecked {
                return (_row.GetHashCode() * 3) ^ _column.GetHashCode();
            }
        }

        public Position AddColumn() {
            return new Position(_row, (char) (_column + 1));
        }

        public Position AddRow() {
            return new Position((char) (_row + 1), _column);
        }

        public Position SubstractColumn() {
            return new Position(_row, (char) (_column - 1));
        }
    }
}