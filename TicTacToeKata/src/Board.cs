using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToeKata {

    public class Board {

        private readonly int _length;
        private Dictionary<Position, Player> _board;
        private char[] _rows;
        private char[] _columns;

        public Board() {
            _length = 3;
            CreateRows();
            CreateColumns();
            CreateBoard();
        }

        private void CreateRows() {
            _rows = new char[_length];
            for (var i = 0; i < _length; i++) _rows[i] = (char) ('A' + i);
        }

        private void CreateColumns() {
            _columns = new char[_length];
            for (var i = 0; i < _length; i++) _columns[i] = (char) ('1' + i);
        }

        private void CreateBoard() {
            _board = new Dictionary<Position, Player>();
            foreach (var row in _rows)
                foreach (var column in _columns)
                    _board[new Position(row, column)] = Player.Null;
        }

        public void TakePosition(Position position, Player player) {
            if (!ExistPosition(position)) throw new IndexOutOfRangeException();
            if (!CanTakePosition(position)) throw new PositionAlreadyTakenException();
            _board[position] = player;
        }

        public bool ExistPosition(Position position) {
            return _board.ContainsKey(position);
        }

        private bool CanTakePosition(Position position) {
            return _board[position] == Player.Null;
        }

        public bool ExistFreePositions() {
            return _board.Values.Any(player => player == Player.Null);
        }
        
        public bool IsThereAWinner() {
            return IsThereARowTakenByAPlayer() || 
                   IsThereAColumnTakenByAPlayer() || 
                   IsThereADiagonalTakenByAPlayer();
        }

        private bool IsThereARowTakenByAPlayer() {
            foreach (var row in _rows)
                if (ArePositionsTakenByTheSamePlayer(_board.Keys.Where(position => position.IsInRow(row)))) return true;
            return false;
        }

        private bool IsThereAColumnTakenByAPlayer() {
            foreach (var column in _columns)
                if (ArePositionsTakenByTheSamePlayer(_board.Keys.Where(position => position.IsInColumn(column)))) return true;
            return false;
        }

        private bool IsThereADiagonalTakenByAPlayer() {
            return ArePositionsTakenByTheSamePlayer(LeftDiagonal()) || ArePositionsTakenByTheSamePlayer(RightDiagonal());
        }
        
        private IEnumerable<Position> LeftDiagonal() {
            var position = new Position(_rows.First(), _columns.First());
            while (ExistPosition(position)) {
                yield return position;
                position = position.AddColumn().AddRow();
            }
        }

        private IEnumerable<Position> RightDiagonal() {
            var position = new Position(_rows.First(), _columns.Last());
            while (ExistPosition(position)) {
                yield return position;
                position = position.SubstractColumn().AddRow();
            }
        }

        private bool ArePositionsTakenByTheSamePlayer(IEnumerable<Position> enumerable) {
            var positions = enumerable.ToList();
            return positions.All(position => _board[position] == Player.O) ||
                   positions.All(position => _board[position] == Player.X);
        }
    }

}