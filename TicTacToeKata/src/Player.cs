namespace TicTacToeKata {

    public class Player {

        public static readonly Player O = new Player("O");
        public static readonly Player X = new Player("X");
        public static readonly Player Null = new Player("");
        private readonly string _player;

        private Player(string player) {
            _player = player;
        }

        public override string ToString() {
            return _player;
        }

    }

}